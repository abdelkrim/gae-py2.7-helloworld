Getting started with Google Appengine and Python 2.7

PREREQUISITE GIT interations: ssh-add ~/.ssh/bitbucketid

===================
VERSIONS
===================
v1-5 Using static files

USE CASES 
- change the body and background colour using a css

===================
v1-4 Using Templates: Jinja2

USE CASES
- user input a content that is displayed like this
"You wrote:

first message"

SYSTEM REQUIREMENTS (changes)
-APIs
 - jinja2
 - os
===================
v1-3 Using the Datastore

USE CASES
-  stores greetings in the Datastore

SYSTEM REQUIREMENTS (changes)
-APIs
 - datetime
 - db
 - urllib
 
===================
v1-2 Handling Forms with webapp2

USE CASES
- user input a content that is displayed like this
"You wrote:

first message"

SYSTEM REQUIREMENTS (changes)
-APIs
 - cgi
===================
v1-1 Using the users services

USE CASES
- print 'Hello, ' + user.nickname()'

SYSTEM REQUIREMENTS (changes)
-APIs
 - Users
===================
v1 Hello World

USE CASES
- print "Hello, webapp2 World!" using Google App Engine

SYSTEM REQUIREMENTS
- GoogleAppEngine 1.7.3
- Eclipse 4.2 Juno
- GIT
- Python 2.7
- Google Plugin for Eclipse 4.2
 
